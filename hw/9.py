#!/usr/bin/env python3

import math

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def length(self):
        return math.hypot(self.x, self.y)

    def __add__(self, rhs):
        return Point(self.x + rhs.x, self.y + rhs.y)

    def __sub__(self, rhs):
        return self + (-rhs)

    def __neg__(self):
        return Point(-self.x, -self.y)

    def __mul__(self, number):
        return Point(self.x * number, self.y * number)

    def __str__(self):
        return '({}; {})'.format(self.x, self.y)


class Func:
    def __init__(self, f, der):
        self._f = f
        self.derivative = der

    def __call__(self, x):
        return self._f(x)

    def __add__(self, rhs):
        def new_f(x):
            return self(x) + rhs(x)
        def new_der(x):
            return self.derivative(x) + rhs.derivative(x)
        return Func(new_f, new_der)

    def __mul__(self, rhs):
        def new_f(x):
            return self(x) * rhs(x)
        def new_der(x):
            return self.derivative(x)*rhs(x) + rhs.derivative(x)*self(x)
        return Func(new_f, new_der)

    @property
    def exp(self):
        def new_f(x):
            return math.exp(self(x))
        def new_der(x):
            return self.derivative(x) * math.exp(self(x))
        return Func(new_f, new_der)

    def __neg__(self):
        def new_f(x):
            return -self(x)
        def new_der(x):
            return -self.derivative(x)
        return Func(new_f, new_der)

    @property
    def ln(self):
        def new_f(x):
            val = self(x)
            return math.log(val) if val > 0 else -1e70
        def new_der(x):
            val = self(x)
            return self.derivative(x) * (1. / val) # if val > 0 else Point(0, 0)
        return Func(new_f, new_der)

def armijo(x, f, alpha_init=8., theta=0.5, eps=0.1):
    alpha = alpha_init
    grad = f.derivative(x)
    grad_len = grad.length()
    val = f(x)
    d = grad * (1 / grad_len)
    while True:
        if f(x - d * alpha) < val - eps * alpha * grad_len:
            # print('alpha_k={} was ok'.format(alpha))
            break
        else:
            # print('alpha_k={} was bad, proceeding'.format(alpha))
            alpha = theta * alpha
    new_x = x - d * alpha
    # print('new x is {} with value {} in it'.format(new_x, f(new_x)))
    return new_x

def penalty_method(x, func, penalty, eps=1e-5):
    func_sum = func + penalty
    i = 0
    log = lambda x: '{} & {} & {} & {} & {} \\\\'.format(i, x, func_sum(x), func(x), penalty(x))

    sigmafunc = Func(lambda p: 1.05, lambda p: Point(0, 0))
    print(log(x))
    while True:
        i += 1
        new = armijo(x, func_sum)
        print(log(new))
        if (x - new).length() < eps and penalty(x) < eps:
            return (x, i)
        x = new
        penalty = penalty * sigmafunc
        func_sum = func + penalty

a1, a2, a3, a4 = 5, 5, 6, 7
b1, b2, b3 = 5, 8, 1
x = Point(-1, -1)
f = Func(
        lambda p: a1*p.x**2 - p.x*p.y/a2 + a3*p.y**2 + a4*p.x,
        lambda p: Point(
            2*a1*p.x - p.y/a2,
            p.x/a2 + 2*a3*p.y + a4))
cons = Func(
        lambda p: b3 - (b1*p.x + b2*p.y),
        lambda p: Point(-b1, -b2))
penalty = -cons.ln

def main():
    ans, steps = penalty_method(x, f, penalty)
    # print(ans, 'after', steps, 'steps')

if __name__ == '__main__':
    main()

