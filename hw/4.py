#!/usr/bin/env python3

STEP_COUNT = 10
L = 200
left_bound, right_bound = -50, 50
a1, a2, a3, a4, a5, a6, a7 = 9, 3, 2, 4, 7, 1, 3

f = lambda x: abs(abs(abs(a1 * x**2 + a2 * x) - abs(a3 * x**2 - a4)) - abs(a5 * x**2 - a6 * x + a7))


class L_f:
    def __init__(self, x0):
        self.a = f(x0)
        self.b = x0

    def __call__(self, x):
        return self.a - L * abs(x - self.b)

    def __or__(self, rhs):
        if self.b > rhs.b:
            self, rhs = rhs, self
        return (self.a - rhs.a) / L + self.b + rhs.b


x = [-3]
curs = [L_f(x[0])]

for i in range(STEP_COUNT):
    ints = [left_bound, right_bound]
    ints.extend(f1|f2 for i, f1 in enumerate(curs) for f2 in curs[i+1:] if left_bound <= (f1|f2) <= right_bound)

    x_new = min(ints, key=lambda x: max(g(x) for g in curs))
    x.append(x_new)
    curs.append(L_f(x_new))
    print(i+1, round(f(x[-1]), 3), round(x[-1], 3), sep=' & ', end=' \\\\\n')

