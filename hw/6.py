#!/usr/bin/env python3

from fractions import Fraction as Frac
import math

a1, a2, a3 = 9, 6, 3
b1, b2 = 7, 7

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def length(self):
        return math.hypot(self.x, self.y)

    def __add__(self, rhs):
        return Point(self.x + rhs.x, self.y + rhs.y)

    def __sub__(self, rhs):
        return self + (-rhs)

    def __neg__(self):
        return Point(-self.x, -self.y)

    def __mul__(self, number):
        return Point(self.x * number, self.y * number)

    def __str__(self):
        return '({}; {})'.format(self.x, self.y)


alpha_init = 1
theta = Frac(1, 2)
eps = Frac(1, 2)

f = lambda p: a1*p.x*p.x - p.x*p.y/a2 + a3*p.y*p.y
f_ = lambda p: Point(
        2*a1*p.x - p.y/a2,
        -p.x/a2 + 2*a3*p.y)

def armijo_from(x):
    print('From point {}:'.format(x))
    alpha = alpha_init
    grad = f_(x)
    grad_len = grad.length()
    val = f(x)
    d = grad * (1 / grad_len)
    while True:
        print('f(x - d * alpha)', f(x - d * alpha))
        print('val - eps * alpha * grad_len', val - eps * alpha * grad_len)
        if f(x - d * alpha) < val - eps * alpha * grad_len:
            print('alpha_k={} was ok'.format(alpha))
            break
        else:
            print('alpha_k={} was bad, proceeding'.format(alpha))
            alpha = theta * alpha
    new_x = x - d * alpha
    print('new x is {} with value {} in it'.format(new_x, f(new_x)))
    return new_x

xs = [Point(b1, b2)]
print('f(x^0):', f(xs[0]))
print('f\'(x^0):', f_(xs[0]))
print('d(x^0):', f_(xs[0]) * (1. / f_(xs[0]).length()))
print('|f\'(x&0)|:', f_(xs[0]).length())
xs.append(armijo_from(xs[-1]))
xs.append(armijo_from(xs[-1]))

