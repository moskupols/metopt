#ifndef SIMPLEX_H
#define SIMPLEX_H

#include "matrix.h"

namespace optimization
{

struct StandardProgram
{
    // Ax = b, c^Tx -> max

    StandardProgram(
            const RealMatrix &A, const RealMatrix &b, const RealMatrix &c);
    StandardProgram(
            const RealMatrix &A,
            const std::vector<double> &b, const std::vector<double> &c);
    StandardProgram(
            const std::vector<std::vector<double>> &A,
            const std::vector<double> &b, const std::vector<double> &c);

    RealMatrix A;
    RealMatrix b;
    RealMatrix c;
};

std::pair<double, std::vector<double>>
optimize(const StandardProgram &P, const std::vector<double> &initial);

std::pair<double, RealMatrix>
optimize(const StandardProgram &P, const RealMatrix &initial);

long long optimize_integer(
        const StandardProgram &P, const std::vector<double> &initial);
long long optimize_integer(StandardProgram P, const RealMatrix &initial);

namespace impl
{
std::pair<double, RealMatrix>
optimize_with_base(const StandardProgram &P, std::vector<std::size_t> initial_base);
} // optimization::impl

} // optimization

#endif // SIMPLEX_H

