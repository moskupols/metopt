#include <iostream>
#include "simplex.h"

using namespace std;

int main()
{
    size_t n, m;
    cin >> m >> n;

    std::vector<std::vector<double>> A(m);
    for (auto &row : A)
    {
        row.resize(n);
        for (double &cell : row)
            cin >> cell;
    }

    std::vector<double> b(m);
    for (double &bi : b)
        cin >> bi;

    std::vector<double> c(n);
    for (double &ci: c)
        cin >> ci;

    std::vector<double> initial(n);
    for (double &x: initial)
        cin >> x;

    optimization::StandardProgram program(A, b, c);
    // long long answer = optimization::optimize_integer(program, initial);
    double answer = optimization::optimize(program, initial).first;

    cout << answer << endl;

    return 0;
}

