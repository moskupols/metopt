#include <cmath>
#include <cassert>
#include <algorithm>

#include "simplex.h"

namespace optimization
{

StandardProgram::StandardProgram(
        const RealMatrix &A, const RealMatrix &b, const RealMatrix &c):
    A(A), b(b), c(c)
{
    assert(b.is_math_vector());
    assert(c.is_math_vector());
    assert(b.rows() == A.rows());
    assert(c.rows() == A.cols());
}
StandardProgram::StandardProgram(
        const RealMatrix &A,
        const std::vector<double> &b, const std::vector<double> &c):
    StandardProgram(A, RealMatrix::math_vector(b), RealMatrix::math_vector(c))
{}

StandardProgram::StandardProgram(
        const std::vector<std::vector<double>> &A,
        const std::vector<double> &b, const std::vector<double> &c):
    StandardProgram(
            RealMatrix(A),
            RealMatrix::math_vector(b), RealMatrix::math_vector(c))
{}


std::pair<double, std::vector<double>>
optimize(const StandardProgram &P, const std::vector<double>&initial)
{
    auto ret = optimize(P, RealMatrix::math_vector(initial));
    return make_pair(ret.first, ret.second.to_std_vector());
}


// Simplex algorithm, as per book by Galeev and Tikhomirov.
std::pair<double, RealMatrix>
optimize(const StandardProgram &P, const RealMatrix &vertex)
{
    using std::size_t;

    assert(vertex.is_math_vector());

    std::vector<size_t> base;
    for (size_t i = 0; i < vertex.rows(); ++i)
        if (std::abs(vertex.at(i)) > EPS)
            base.push_back(i);
    for (size_t i = 0; i < vertex.rows() && base.size() < P.A.rows(); ++i)
        if (std::abs(vertex.at(i)) <= EPS)
            base.push_back(i);

    return impl::optimize_with_base(P, base);
}

long long optimize_integer(
        const StandardProgram &P, const std::vector<double> &initial)
{
    return optimize_integer(P, RealMatrix::math_vector(initial));
}

// Gomory method, as per
// https://en.wikipedia.org/wiki/Cutting-plane_method
long long optimize_integer(StandardProgram P, const RealMatrix &initial)
{
    using std::size_t;

    auto frac = [](double d){ return d - std::floor(d); };

    while (true)
    {
        std::pair<double, RealMatrix> exact = optimize(P, initial);
        if (std::isinf(exact.first)) // unbound problem
          return std::numeric_limits<long long>::max();

        RealMatrix &x = exact.second;

        size_t worst_x = 0;
        for (size_t i = 1; i < x.rows(); ++i)
            if (frac(x.at(i)) > frac(x.at(worst_x)))
                worst_x = i;

        if (frac(x.at(worst_x)) < EPS)
            return (long long)exact.first;

        std::vector<double> new_constraint;
        new_constraint.reserve(P.A.cols() + 1);

        for (size_t i = 0; i < P.A.cols(); ++i)
            new_constraint.push_back(frac(P.A.at(worst_x, i)));
        new_constraint.push_back(1.);

        P.A.add_column(std::vector<double>(P.A.rows(), 0.));
        P.A.add_row(new_constraint);

        P.b.add_row(std::vector<double>(1, frac(P.b.at(worst_x))));
        P.c.add_row(std::vector<double>(1, 0.));
    }
}

namespace impl
{
using std::size_t;

std::pair<double, RealMatrix>
optimize_with_base(const StandardProgram &P, std::vector<size_t> base)
{
    assert(base.size() == P.A.rows());
    assert(*max_element(base.begin(), base.end()) < P.A.cols());

    while (true) {
        std::vector<size_t> out_of_base;
        for (size_t i = 0; i < P.A.cols(); ++i)
            if (!std::count(base.begin(), base.end(), i))
                out_of_base.push_back(i);

        RealMatrix base_inverse = // inverse of base submatrix
            RealMatrix(base.size(), base.size(),
                [P, base](size_t r, size_t c)
                { return P.A.at(r, base[c]); })
            .inversed();

        RealMatrix base_costs(1, base.size(),
                [P, base](size_t, size_t c) { return P.c.at(base[c]); }); // it's a row

        RealMatrix base_solution = base_inverse * P.b;
        RealMatrix u = base_costs * base_inverse;
        RealMatrix reduced_cost = P.c - (u * P.A).transposed();

        // Column of reduced cost with min value
        size_t p = out_of_base[0];
        for (size_t r : out_of_base)
            if (reduced_cost.at(r) < reduced_cost.at(p))
                p = r;
        bool optimal = reduced_cost.at(p) > -EPS;

        if (!optimal) {
            RealMatrix column_p(P.A.rows(), 1,
                    [P, p](size_t r, size_t) { return P.A.at(r, p); });
            RealMatrix a_tilde = base_inverse * column_p;

            bool unlimited = true;
            for (size_t i = 0; i < a_tilde.rows() && unlimited; ++i)
                unlimited = unlimited && a_tilde.at(i) < 1e6;

            if (!unlimited) {
                int q_pos = -1;
                for (size_t i = 0; i < base.size(); ++i) {
                    long double value = base_solution.at(i) / a_tilde.at(i);
                    if ( a_tilde.at(i) > 0 &&
                            ( q_pos == -1 || value < ( base_solution.at(q_pos) / a_tilde.at(q_pos))))
                        q_pos = i;
                }
                size_t q = base[q_pos];
                std::replace(base.begin(), base.end(), q, p);
            } else {
                // Problem is unlimited
                return std::make_pair(std::numeric_limits<double>::infinity(),
                        RealMatrix());
            }
        } else {
            // Optimal solution is found!!11!!!

            RealMatrix objective_function_base(1, base.size(),
                    [P, base](size_t r, size_t)
                    { return P.c.at(base[r]); });
            RealMatrix full_solution(P.A.cols(), 1, 0.);

            for (size_t i = 0; i < P.A.cols(); ++i)
            {
                size_t idx = std::find(base.begin(), base.end(), i) - base.begin();
                if (idx != base.size())
                    full_solution.at(i) = base_solution.at(idx);
            }
            double solution_value = (objective_function_base * base_solution).value();

            return std::make_pair(solution_value, full_solution);
        }
    }
}
} // namespace optimization::impl
} // namespace optimization

