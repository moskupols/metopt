#ifndef MATRIX_H
#define MATRIX_H

#include <vector>
#include <cassert>
#include <cmath>

namespace optimization
{
extern double EPS;

template<typename T>
class Matrix
{
public:
    typedef T value_type;
    typedef std::size_t size_t;

    // Constructors

    Matrix() {}

    explicit Matrix(const std::vector<std::vector<T>> &v):
        v(with_checked_dimensions(v))
    {}

    Matrix(size_t rows, size_t cols, T filler):
        v(rows, row_t(cols, filler))
    {}

    template<class F>
    Matrix(size_t rows, size_t cols, F filler):
        v(smart_filled(rows, cols, filler))
    {}

    static Matrix eye(size_t size)
    {
        return Matrix(size, size,
                [](size_t r, size_t c) { return (r == c ? 1 : 0); });
    }

    static Matrix math_vector(const std::vector<T> &value)
    {
        assert(!value.empty());
        return Matrix(value.size(), 1,
                [value](size_t r, size_t) { return value[r]; });
    }

    // Getters

    bool is_math_vector() const { return cols() == 1; }

    T value() const
    {
        assert(rows() == 1);
        assert(cols() == 1);
        return at(0, 0);
    }

    size_t rows() const { return v.size(); }
    size_t cols() const { return v.back().size(); }

    T& at(size_t r)
    {
        assert(is_math_vector());
        return v[r][0];
    }
    T const& at(size_t r) const
    {
        assert(is_math_vector());
        return v[r][0];
    }
    T& at(size_t r, size_t c) { return v[r][c]; }
    T const& at(size_t r, size_t c) const { return v[r][c]; }

    // Operations

    Matrix transposed() const
    {
        return Matrix(cols(), rows(),
                [this](size_t r, size_t c) { return v[c][r]; });
    }

    Matrix operator*(const Matrix &rhs) const
    {
        assert(cols() == rhs.rows());
        return Matrix(rows(), rhs.cols(),
                [this, rhs](size_t r, size_t c)
                {
                    T ret = T();
                    for (size_t i = 0; i < cols(); ++i)
                        ret += at(r, i) * rhs.at(i, c);
                    return ret;
                });
    }
    Matrix operator-(const Matrix &rhs) const
    {
        assert(cols() == rhs.cols());
        assert(rows() == rhs.rows());
        return Matrix(rows(), rhs.cols(),
                [this, rhs](size_t r, size_t c)
                { return at(r, c) - rhs.at(r, c); });
    }

    bool equals(const Matrix &rhs) const
    {
        return equals(rhs, std::equal<T>);
    }
    template<class Pred>
    bool equals(const Matrix &rhs, Pred eq) const
    {
        if (rows() != rhs.rows() || cols() != rhs.cols())
            return false;
        for (size_t i = 0; i < rows(); ++i)
            for (size_t j = 0; j < cols(); ++j)
                if (!eq(at(i, j), rhs.at(i, j)))
                    return false;
        return true;
    }

    Matrix& add_column(const std::vector<T> &c)
    {
        if (v.empty())
            return (*this = Matrix::math_vector(c));
        assert(c.size() == rows());
        for (size_t i = 0; i < rows(); ++i)
            v[i].push_back(c[i]);
        return *this;
    }

    Matrix& add_row(const std::vector<T> &r)
    {
        if (v.empty())
        {
            assert(!r.empty());
            v.push_back(r);
            return *this;
        }
        assert(r.size() == cols());
        v.push_back(r);
        return *this;
    }

    std::vector<T> to_std_vector() const
    {
        assert(is_math_vector());
        std::vector<T> ret;
        ret.reserve(rows());
        for (size_t r = 0; r < rows(); ++r)
            ret.push_back(at(r, 0));
        return ret;
    }



    Matrix<T> inversed() const
    {
        assert(rows() == cols());

        Matrix<T> left = *this;
        Matrix<T> right = Matrix<T>::eye(rows());

        // Gaussian elimination time!
        for (size_t row = 0, col = 0; row < rows() && col < cols(); ++col, ++row)
        {
            size_t sr = row;
            for (size_t i = row; i < rows(); ++i)
                if (std::abs(left.at(i, col)) > std::abs(left.at(sr, col)))
                    sr = i;
            assert(std::abs(left.at(sr, col)) >= EPS); // linear dependancy check

            left.v[row].swap(left.v[sr]);
            right.v[row].swap(right.v[sr]);
            for (size_t i = 0; i < rows(); ++i)
                if (i != row)
                {
                    double c = left.at(i, col) / left.at(row, col);
                    for (size_t j = col; j < cols(); ++j)
                        left.at(i, j) -= left.at(row, j) * c;
                    for (size_t j = 0; j < cols(); ++j)
                        right.at(i, j) -= right.at(row, j) * c;
                }
        }
        // now left should be diagonal, but not necessary eye
        // normalize
        for (size_t r = 0; r < rows(); ++r)
            for (size_t c = 0; c < cols(); ++c)
                right.at(r, c) /= left.at(r, r);
        return right;
    }

protected:
    typedef std::vector<T> row_t;
    typedef std::vector<row_t> internal_t;

    static internal_t with_checked_dimensions(const internal_t &v)
    {
        assert(!v.empty());
        assert(!v.back().empty());
        for (row_t row : v)
            assert(row.size() == v.back().size());
        return v;
    }

    template<class F>
    static internal_t smart_filled(size_t rows, size_t cols, F filler)
    {
        internal_t ret(rows, row_t());
        for (size_t r = 0; r < rows; ++r)
        {
            row_t &row = ret[r];
            row.reserve(cols);
            for (size_t c = 0; c < cols; ++c)
                row.push_back(filler(r, c));
        }
        return ret;
    }

private:
    internal_t v;
};

typedef Matrix<double> RealMatrix;

}

#endif // MATRIX_H

